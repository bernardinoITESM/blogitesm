<?php namespace BlogITESM;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Faker\Factory;

use BlogITESM\User;

class UserTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
        $faker=Factory::create();

        foreach(range(0,4) as $index){
            User::create(
                ['name'=>$faker->name(),
                'password'=>'qwerty123',
                'email'=>$faker->email]
            );

        }

		// $this->call('UserTableSeeder');
	}

}
