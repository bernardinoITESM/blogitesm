<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserToPost extends Migration {

	/**
	 * Run the migrations.
     * This migration added the column "user_id" to Posts table and below to that, the foreign key is added in order to create a kind of relationship one-to-many
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('posts', function($table){
            $table->string('user_id',36);
            $table->foreign('user_id')->references('id')->on('users')->ondelete('cascade');

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('posts', function($table) {
            $table->dropforeign('posts_user_id_foreign');
            $table->dropColumn('user_id');

        });
	}
}
