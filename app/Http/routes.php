<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

/*
 * Make the route for User resource
 * */

Route::group(['prefix'=>'api/v1'], function(){
    Route::resource('users','UserController', ['except'=>'store']);
    Route::post('signup','UserAuthController@store', ['only'=>'store']);
    Route::post('signin','UserAuthController@signin', ['only'=>'signin']);
    Route::resource('users.post','UserPostController');

    Route::resource('post','UserPostController@show',['only'=>'show']);

    //All the post
    Route::resource('allpost','PostController@index',['only'=>'index']);
});


