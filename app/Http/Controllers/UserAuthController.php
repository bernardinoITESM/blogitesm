<?php namespace BlogITESM\Http\Controllers;

use BlogITESM\Http\Requests;
use BlogITESM\Http\Controllers\Controller;

use BlogITESM\Http\Requests\UserRequest; 
use BlogITESM\User;

use HttpResponse;
use Illuminate\Http\Response;
use Tymon\JWTAuth\Facades\JWTAuth;
use BlogITESM\Http\Requests\UserSignInRequest;

class UserAuthController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(UserRequest $request)
    {
        //
        $vals = $request->all();

        try {
            $user = User::create($vals);

		    if (!$user) {
                return response()->json(['message' => 'Something was wrong, try later'], Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            $token = JWTAuth::fromUser($user);
            return response()->json(['user' => $user, 'token'=>$token], Response::HTTP_CREATED);
        }catch(\Exception $e){
            return response()->json(['message' => 'the user already exist '.$e], Response::HTTP_CONFLICT);
        }

	}

    public function signin(UserSignInRequest $request){
        $vals = $request->all();
        if ( ! $token = JWTAuth::attempt($vals)) {
            return Response::json(false, Response::HTTP_UNAUTHORIZED);
        }

        $token=compact('token');
        return response()->json($token, Response::HTTP_OK);
    }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
