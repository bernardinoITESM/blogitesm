<?php namespace BlogITESM\Http\Controllers;

use BlogITESM\Http\Requests;
use BlogITESM\Http\Controllers\Controller;

use BlogITESM\Post;
use Illuminate\Http\Request;

use Symfony\Component\HttpFoundation\Response;

class PostController extends Controller {

    public function __construct()
    {
        $this->middleware('jwt.auth');
    }


    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
        $posts=Post::all();
        if(!$posts){
            return response()->json(['message'=>'Not results'],Response::HTTP_NO_CONTENT);
        }

        return response()->json(['posts'=>$posts],Response::HTTP_OK);
	}


}
