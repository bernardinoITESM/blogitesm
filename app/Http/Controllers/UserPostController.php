<?php namespace BlogITESM\Http\Controllers;

use BlogITESM\Http\Requests;
use BlogITESM\Http\Controllers\Controller;

use BlogITESM\User;
use Illuminate\Http\Request;
use BlogITESM\Http\Requests\PostRequest;
use Illuminate\Http\Response;
use BlogITESM\Post;


class UserPostController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($id)
	{
		//

        //Find a User with $userId
        $usr=User::find($id);


        //If the user is not in DB
        if(!$usr){
            return response()->json(['message'=>'User not found'],Response::HTTP_NOT_FOUND);
        }

        //we try to create a post and retrieve it
        return response()->json(['post'=>$usr->posts],Response::HTTP_CREATED);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(PostRequest $request, $userId)
	{
        //get all values in the request
        $vals=$request->all();
		//Find a User with $userId
        $usr=User::find($userId);


        //If the user is not in DB
        if(!$usr){
            return response()->json(['message'=>'User not found'],Response::HTTP_NOT_FOUND);
        }

        //we try to create a post and retrieve it
        $post=$usr->posts()->create($vals);
        return response()->json(['message'=>'post created','post'=>$post],Response::HTTP_CREATED);

    }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $post = Post::find($id);

        if(!$post){
            return response()->json(['message'=>'Post not found'],Response::HTTP_NOT_FOUND);
        }

        return response()->json(['post '=>$post], Response::HTTP_OK);
    }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
