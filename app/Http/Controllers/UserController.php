<?php namespace BlogITESM\Http\Controllers;

use BlogITESM\Http\Requests;
use BlogITESM\Http\Controllers\Controller;

use Illuminate\Http\Request;

use BlogITESM\User;
use BlogITESM\Http\Requests\UserRequest;

class UserController extends Controller {

    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
        $users=User::all();

        if(!$users){
            return response()->json(['message'=>'Users empty'],200);

        }


        return response()->json(['users'=>$users],200);

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(UserRequest $request)
	{
		//
        return response()->json(['request'=>'Hola'],201);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
        $user=User::find($id);
        if(!$user){
            return response()->json(['message'=>'Users empty'],200);

        }
        return response()->json(['users'=>$user],200);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
