<?php namespace BlogITESM;

use Illuminate\Database\Eloquent\Model;

class Post extends UuidModel {

    protected $fillable = ['title', 'content'];

    //
    public function users(){
        return $this->belongsTo('BlogITESM\User');
    }


}
